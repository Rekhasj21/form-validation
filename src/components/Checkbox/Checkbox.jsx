import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addUser, clearFormData } from "../FormValidationSlice";

function Checkbox({ onBack }) {
  const [isFormSubmitted, setIsFormSubmitted] = useState(false);
  const { gender, isCheckboxSelected } = useSelector((store) => store.formData);
  const { formData } = useSelector((store) => store);
  const [error, setError] = useState({});

  const dispatch = useDispatch();

  const handleInputChange = (value) => {
    dispatch(addUser({ name: "gender", value }));
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    const formErrors = validateForm({ gender, isCheckboxSelected });
    setError(formErrors);
    setIsFormSubmitted(true);
    dispatch(clearFormData(formData));
  };
  const validateForm = ({ gender, isCheckboxSelected }) => {
    let formError = {};
    if (!gender) {
      formError.gender = "Select correct Gender";
    }
    if (!isCheckboxSelected) {
      formError.isCheckboxSelected = "Choose correct option";
    }
    return formError;
  };
  const handleOptionChange = (event) => {
    const isChecked = event.target.value;
    dispatch(addUser({ name: "isCheckboxSelected", value: isChecked }));
  };

  return (
    <>
      {Object.keys(error).length === 0 &&
        isFormSubmitted &&
        alert(`Submitted details of ${formData.firstName}`)}
      <div className="mt-5 ms-5">
        <span className="fs-6 text-secondary text-opacity-50">Step3/3</span>
        <h1 className="fs-4 fw-bold ">Checkbox</h1>
      </div>
      <form className="row g-3 pe-5 ms-5" onSubmit={handleSubmit}>
        <div className="">
          <button
            type="button"
            value={gender === "Male" ? "" : "Male"}
            className={`btn  m-2 pe-2 ps-2 pt-4 pb-4 ${
              gender === "Male"
                ? "bg-primary bg-opacity-10 border-primary"
                : "bg-secondary bg-opacity-10 border-secondary"
            }`}
            onClick={() => handleInputChange("Male")}
          >
            <img src="/male-icon.png" alt="male-icon" className="me-3 w-25" />
          </button>
          <button
            type="button"
            value={gender === "Female" ? "" : "Female"}
            className={`btn  m-2 pe-5 ps-5 pt-3 pb-3 ${
              gender === "Female"
                ? "bg-primary bg-opacity-10 border-primary"
                : "bg-secondary bg-opacity-10 border-secondary"
            }`}
            onClick={() => handleInputChange("Female")}
          >
            <img
              src="/female-icon.png"
              alt="female-icon"
              className="me-2"
              style={{ width: "50px" }}
            />
          </button>
          <p className="text-danger ms-1">{error.gender}</p>
          <div className="form-check m-1">
            <input
              type="radio"
              className="form-check-input"
              id="option"
              value="option"
              name="option"
              checked={isCheckboxSelected === "option"}
              onChange={handleOptionChange}
            />
            <label htmlFor="option" className="form-check-label">
              I want to add this option
            </label>
          </div>
          <div className="form-check m-1">
            <input
              type="radio"
              className="form-check-input"
              id="coolStuff"
              value="coolStuff"
              name="option"
              checked={isCheckboxSelected === "coolStuff"}
              onChange={handleOptionChange}
            />
            <label htmlFor="coolStuff" className="form-check-label">
              Let me click on this checkbox and choose some cool stuf
            </label>
          </div>
          <p className="text-danger ms-1">{error.isCheckboxSelected}</p>
          <hr className="text-success mt-4 w-75" />
          <div className="d-flex justify-content-end pt-4 me-5">
            <button className="btn fw-bold me-3" onClick={onBack}>
              Back
            </button>
            <button
              className="btn btn-primary ms-3 p-2 pe-4 ps-4"
              type="submit"
            >
              Submit
            </button>
          </div>
        </div>
      </form>
    </>
  );
}

export default Checkbox;
