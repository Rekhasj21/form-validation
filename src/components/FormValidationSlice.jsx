import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  firstName: "",
  lastName: "",
  dob: "",
  email: "",
  address: "",
  message: "",
  gender: "",
  isMessageSelected:"",
  isCheckboxSelected:""
};

const FormValidationSlice = createSlice({
  name: "forms",
  initialState,
  reducers: {
    addUser: (state, { payload }) => {
      state[payload.name] = payload.value;
    },
    clearFormData: (state) => {
      Object.assign(state, initialState);
    },
  },
});

export const { addUser, clearFormData } = FormValidationSlice.actions;

export default FormValidationSlice.reducer;
