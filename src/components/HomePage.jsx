import React from "react";
import Navbar from "../components/Navbar/Navbar";
import SignupPage from "../components/Singup/SignupPage";
import MessagesPage from "../components/Messages/MessagesPage";
import Checkbox from "./Checkbox/Checkbox";
import { useSelector, useDispatch } from "react-redux";

function HomePage() {
  const [currentPage, setCurrentPage] = React.useState("signup");
  const { formData } = useSelector((store) => store);

  const handleNext = () => {
    switch (currentPage) {
      case "signup":
        setCurrentPage("messages");
        break;
      case "messages":
        setCurrentPage("checkbox");
        break;
      case "checkbox":
        setCurrentPage("submit");
        break;
      default:
        setCurrentPage("signup");
    }
  };

  const handleBack = () => {
    switch (currentPage) {
      case "messages":
        setCurrentPage("signup");
        break;
      case "checkbox":
        setCurrentPage("messages");
        break;
      default:
        setCurrentPage("signup");
    }
  };

  return (
    <>
      <div className="d-flex shadow m-5 rounded bg-light h-100">
        <img
          src={`/${currentPage}.avif`}
          className=""
          style={{ width: "25rem" }}
        />
        <div className=" m-5" style={{ width: "50rem" }}>
          <div>
            <Navbar currentPage={currentPage} />
            {currentPage === "signup" && <SignupPage onNext={handleNext} />}
            {currentPage === "messages" && (
              <MessagesPage onBack={handleBack} onNext={handleNext} />
            )}
            {currentPage === "checkbox" && (
              <Checkbox onNext={handleNext} onBack={handleBack} />
            )}
          </div>
        </div>
      </div>
    </>
  );
}

export default HomePage;
