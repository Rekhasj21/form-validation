import { configureStore } from "@reduxjs/toolkit";
import FormValidationSliceReducer from "./components/FormValidationSlice";

export const store = configureStore({
  reducer: {
    formData: FormValidationSliceReducer,
  },
});
