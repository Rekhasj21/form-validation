import React from "react";
import { TiTick } from "react-icons/ti";

function Navbar({ currentPage }) {
  currentPage = currentPage;

  return (
    <nav className="navbar shadow-sm p-1 w-75 ms-5">
      <div
        className="btn-group mb-3 mt-1"
        role="group"
        aria-label="Basic checkbox toggle button group"
      >
        <label
          className={`btn rounded  ${
            currentPage === "signup"
              ? "bg-primary text-white"
              : "bg-secondary  bg-opacity-25"
          }`}
          htmlFor="signup"
        >
          {currentPage === "signup" ? (
            1
          ) : (
            <TiTick fill="blue" fontSize="20px" />
          )}
        </label>
        <input
          type="checkbox"
          className="btn-check"
          id="signup"
          autoComplete="off"
        />
        <label className="btn">Signup</label>
        <label
          className={`btn rounded  ${
            currentPage === "messages"
              ? "bg-primary text-white"
              : "bg-secondary  bg-opacity-25"
          }`}
          htmlFor="message"
        >
          {(currentPage === "messages" || currentPage === "signup") && 2}
          {currentPage === "checkbox" && <TiTick fill="blue" fontSize="20px" />}
        </label>
        <input
          type="checkbox"
          className="btn-check"
          id="message"
          autoComplete="off"
        />
        <label className="btn">Message</label>
        <label
          className={`btn rounded  ${
            currentPage === "checkbox"
              ? "bg-primary text-white"
              : "bg-secondary  bg-opacity-25"
          }`}
          htmlFor="checkbox"
        >
          3
          {/* {(currentPage === "messages" || currentPage === "signup") && 3} */}
          {/* {currentPage === "checkbox" && <TiTick fill="blue" fontSize="20px" />} */}
        </label>
        <input
          type="checkbox"
          className="btn-check"
          id="checkbox"
          autoComplete="off"
        />
        <label className="btn">Checkbox</label>
      </div>
    </nav>
  );
}

export default Navbar;
