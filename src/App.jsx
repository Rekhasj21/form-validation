import React from "react";
import HomePage from "./components/HomePage";

function App() {
  return (
    <div className="d-flex flex-column justify-content-center align-items-center p-5" style={{height:"100vh"}}>
      <HomePage />
    </div>
  );
}

export default App;
