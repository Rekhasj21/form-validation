import React, { useState, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { addUser } from "../FormValidationSlice";
import { RiErrorWarningLine } from "react-icons/ri";

function SignupPage({ onNext }) {
  const dispatch = useDispatch();
  const [error, setError] = useState({});
  const [isSigned, setIsSigned] = useState(false);
  const { firstName, lastName, dob, email, address } = useSelector(
    (store) => store.formData
  );
  const handleInputChange = (event) => {
    const { name, value } = event.target;
    dispatch(addUser({ name, value }));
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    const formErrors = validateForm({
      firstName,
      lastName,
      dob,
      email,
      address,
    });
    setError(formErrors);
    setIsSigned(true);
  };

  useEffect(() => {
    if (Object.keys(error).length === 0 && isSigned) {
      onNext();
    }
  }, [error, isSigned]);

  const validateForm = ({ firstName, lastName, dob, email, address }) => {
    const emailformat = /^[^\s@]+@[^\s@]+\.[^\s@]{2,}$/i;
    let formError = {};
    if (!firstName) {
      formError.firstName = "Enter correct firstName";
    } else if (firstName.length < 4) {
      formError.firstName = "Name should be atleast 4 characters";
    }
    if (!lastName) {
      formError.lastName = "Enter correct lastName";
    } else if (lastName.length < 4) {
      formError.lastName = "Name should be atleast 4 characters";
    }
    if (!email) {
      formError.email = "Enter correct email";
    } else if (!emailformat.test(email)) {
      formError.email = "Wrong email ID";
    }
    if (!address) {
      formError.address = "Enter correct address";
    } else if (address.length < 10) {
      formError.address = "Address should be atleast 10 characters";
    }
    if (!dob) {
      formError.dob = "Enter correct DOB";
    }
    return formError;
  };

  return (
    <>
      <div className="mt-5 ms-5">
        <span className="fs-6 text-secondary text-opacity-50">Step1/3</span>
        <h1 className="fs-4 fw-bold">Sign UP</h1>
      </div>
      <form className="row g-3 ms-5" onSubmit={handleSubmit}>
        <div className={`col-md-4 ${error.firstName && "text-danger"}`}>
          <label htmlFor="firstName" className="form-label">
            First name
          </label>
          <div className="d-flex position-relative">
            <input
              type="text"
              className={`form-control  ${
                error.firstName && "bg-danger bg-opacity-10 border-danger"
              }`}
              id="firstName"
              value={firstName}
              name="firstName"
              onChange={handleInputChange}
            />
            {error.firstName && (
              <RiErrorWarningLine className="position-absolute bottom-0 end-0 m-2 mb-2" />
            )}
          </div>
          <p className="text-danger">{error.firstName}</p>
        </div>
        <div className={`col-md-4 me-2 ${error.lastName && "text-danger"}`}>
          <label htmlFor="lastName" className="form-label">
            Last name
          </label>
          <div className="d-flex position-relative">
            <input
              type="text"
              className={`form-control  ${
                error.lastName && "bg-danger bg-opacity-10 border-danger"
              }`}
              id="lastName"
              value={lastName}
              name="lastName"
              onChange={handleInputChange}
            />
            {error.lastName && (
              <RiErrorWarningLine className="position-absolute bottom-0 end-0 m-2 mb-2" />
            )}
          </div>
          <p className="text-danger">{error.lastName}</p>
        </div>
        <div className={`col-md-4 ${error.dob && "text-danger"}`}>
          <label htmlFor="dob" className="form-label">
            Date of Birth
          </label>
          <div className="d-flex position-relative">
            <input
              type="date"
              className={`form-control  ${
                error.dob && "bg-danger bg-opacity-10 border-danger pe-4"
              }`}
              id="dob"
              value={dob}
              name="dob"
              onChange={handleInputChange}
            />
            {error.dob && (
              <RiErrorWarningLine className="position-absolute bottom-0 end-0 mb-2 me-1" />
            )}
          </div>

          <p className="text-danger">{error.dob}</p>
        </div>
        <div className={`col-md-4 ${error.email && "text-danger"}`}>
          <label htmlFor="email" className="form-label">
            Email Address
          </label>
          <div className="d-flex position-relative">
            <input
              className={`form-control  ${
                error.email && "bg-danger bg-opacity-10 border-danger"
              }`}
              id="email"
              value={email}
              name="email"
              onChange={handleInputChange}
            />
            {error.email && (
              <RiErrorWarningLine className="position-absolute bottom-0 end-0 m-2 mb-2" />
            )}
          </div>
          <p className="text-danger">{error.email}</p>
        </div>
        <div className={`col-8 ${error.address && "text-danger"}`}>
          <label className="form-check-label" htmlFor="address">
            Address
          </label>
          <div className="d-flex position-relative">
            <input
              className={`form-control  ${
                error.address && "bg-danger bg-opacity-10 border-danger"
              }`}
              type="text"
              name="address"
              value={address}
              id="address"
              onChange={handleInputChange}
            />
            {error.address && (
              <RiErrorWarningLine className="position-absolute bottom-0 end-0 m-2 mb-2" />
            )}
          </div>
          <p className="text-danger">{error.address}</p>
        </div>
        <hr className="text-success ms-4 mt-4 w-50" />
        <div className="d-flex justify-content-center align-items-end ms-5">
          <button type="submit" className="btn btn-primary p-2 pe-4 ps-4">
            Next Step
          </button>
        </div>
      </form>
    </>
  );
}

export default SignupPage;
