import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { addUser } from "../FormValidationSlice";
import { RiErrorWarningLine } from "react-icons/ri";

function MessagesPage({ onBack, onNext }) {
  const { message, isMessageSelected } = useSelector((store) => store.formData);
  const [error, setError] = useState({});
  const [isSigned, setIsSigned] = useState(false);
  const dispatch = useDispatch();

  const handleInputChange = (event) => {
    const { name, value } = event.target;
    dispatch(addUser({ name, value }));
  };
  const handleSubmit = (event) => {
    event.preventDefault();
    const formErrors = validateForm({ message, isMessageSelected });
    setError(formErrors);
    setIsSigned(true);
  };
  const validateForm = ({ message, isMessageSelected }) => {
    let formError = {};
    if (!message) {
      formError.message = "Enter correct message";
    } else if (message.length < 10) {
      formError.message = "Message should be atleast 10 characters";
    }
    if (!isMessageSelected) {
      formError.isMessageSelected = "Choose option correctly";
    }
    return formError;
  };

  useEffect(() => {
    if (Object.keys(error).length === 0 && isSigned) {
      onNext();
    }
  }, [error, isSigned]);

  const handleOptionChange = (event) => {
    console.log(event.target.value);
    const isChecked = event.target.value;
    dispatch(addUser({ name: "isMessageSelected", value: isChecked }));
  };
  return (
    <>
      <div className="mt-5 ms-5">
        <span className="fs-6 text-secondary text-opacity-50">Step2/3</span>
        <h1 className="fs-4 fw-bold mb-4">Message</h1>
      </div>
      <form className="row g-3 pe-5 ms-5" onSubmit={handleSubmit}>
        <div className={`${error.message && "text-danger"}`}>
          <label htmlFor="message">Message</label>
          <div className="form-floating position-relative">
            <textarea
              rows={6}
              className={`w-75 h-25 ${
                error.message && "bg-danger bg-opacity-10 border-danger"
              }`}
              placeholder=""
              id="message"
              name="message"
              value={message}
              style={{ height: 100 }}
              onChange={handleInputChange}
            />
            {error.message && (
              <RiErrorWarningLine className="position-absolute bottom-0 me-5" />
            )}
            <p className="text-danger">{error.message}</p>
          </div>
        </div>
        <div className="d-flex ">
          <div className="form-check m-1">
            <input
              type="radio"
              className="form-check-input"
              id="choice1"
              name="option"
              value="choice1"
              checked={isMessageSelected === "choice1"}
              onChange={handleOptionChange}
            />
            <label htmlFor="choice1" className="form-check-label">
              The number one choice
            </label>
          </div>
          <div className="form-check m-1">
            <input
              type="radio"
              className="form-check-input"
              id="choice2"
              value="choice2"
              name="option"
              checked={isMessageSelected === "choice2"}
              onChange={handleOptionChange}
            />
            <label htmlFor="choice2" className="form-check-label">
              The number two choice
            </label>
          </div>
        </div>
        <p className="text-danger">{error.isMessageSelected}</p>
        <hr className="text-success mt-3 w-75" />
        <div className="d-flex justify-content-center ms-5">
          <button className="btn fw-bold m-2" onClick={onBack}>
            Back
          </button>
          <button className="btn btn-primary p-2 pe-4 ps-4" type="submit">
            Next Step
          </button>
        </div>
      </form>
    </>
  );
}

export default MessagesPage;
